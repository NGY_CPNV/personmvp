﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMVP
{
    public class Person
    {
        #region private attributs
        private string firstname;
        private string lastname;
        #endregion private attributs

        #region constructors
        public Person() { }
        #endregion constructors

        #region accessors and mutators
        public string Firstname
        {
            get
            {
                return this.firstname;
            }
            set
            {
                this.firstname = value;
            }
        }

        public string Lastname
        {
            get
            {
                return this.lastname;
            }
            set
            {
                this.lastname = value;
            }
        }
        #endregion accessors and mutators
    }
}
