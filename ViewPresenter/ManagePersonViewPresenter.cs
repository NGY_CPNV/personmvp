﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMVP
{
    public class ManagePersonViewPresenter : IManagePersonViewPresenter
    {
        private readonly IManagePersonView view;

        #region constructors
        public ManagePersonViewPresenter(IManagePersonView view)
        {
            this.view = view;
        }

        public void Start()
        {
            this.view.AttachPresenter(this);
        }
        #endregion constructors
        
        #region public method
        public void SavePerson()
        {
            var Person = view.Person;

            //code to save person
        }
        #endregion public method
    }
}
