﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonMVP
{
    public partial class ManagePersonView : Form, IManagePersonView
    {
        #region private attributs
        private Person person = new Person();
        private IManagePersonViewPresenter presenter;
        #endregion private attributs

        #region constructors
        public ManagePersonView()
        {
            InitializeComponent();
        }
        #endregion constructors

        #region accessors and mutators
        public Person Person
        {
            get
            {
                return this.person;
            }
            set
            {
                this.person = value;

                this.txtBFirstname.Text = this.person.Firstname;
                this.txtBLastname.Text = this.person.Lastname;
            }
        }
        #endregion accessors and mutators

        public void AttachPresenter(IManagePersonViewPresenter presenter)
        {
            this.presenter = presenter;
        }

        #region Events Handlers
        private void CmdSave_Click(object sender, EventArgs e)
        {
            this.person.Firstname = this.txtBFirstname.Text;
            this.person.Lastname = this.txtBLastname.Text;
            this.presenter.SavePerson();
        }
        #endregion Events Handlers
    }
}
