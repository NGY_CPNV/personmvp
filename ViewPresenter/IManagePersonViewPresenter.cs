﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMVP
{
    public interface IManagePersonViewPresenter
    {
        #region 
        void Start();
        void SavePerson();
        #endregion
    }
}
