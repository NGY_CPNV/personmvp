﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonMVP
{
    public interface IManagePersonView
    {
        Person Person { get; set; }
        void AttachPresenter(IManagePersonViewPresenter presenter);
    }
}
